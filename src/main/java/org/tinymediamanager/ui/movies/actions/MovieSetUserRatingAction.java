/*
 * Copyright 2012 - 2022 Manuel Laggner
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.tinymediamanager.ui.movies.actions;

import org.tinymediamanager.core.TmmResourceBundle;
import org.tinymediamanager.core.entities.MediaRating;
import org.tinymediamanager.core.movie.entities.Movie;
import org.tinymediamanager.scraper.MediaMetadata;
import org.tinymediamanager.ui.MainWindow;
import org.tinymediamanager.ui.actions.TmmAction;
import org.tinymediamanager.ui.movies.MovieUIModule;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The MovieDeleteAction - to remove all selected movies from the database and from the datasource
 * 
 * @author Manuel Laggner
 */
public class MovieSetUserRatingAction extends TmmAction {
  private static final long serialVersionUID = -984567332370801731L;
  private int userRating;
  public MovieSetUserRatingAction(int userRating) {
    this.userRating = userRating;
    String name;
    if (userRating == 0) {
      name = "清除用户评分";
    } else if (userRating == -1) {
      name = "降低用户评分";
    } else if (userRating == -2) {
      name = "提高用户评分";
    } else {
      name = "设置用户评分 " + userRating;
    }
    putValue(NAME, name);
    putValue(SHORT_DESCRIPTION, name);
    int key = KeyEvent.VK_0 + userRating;
    if (userRating == -1) {
      key = KeyEvent.VK_MINUS;
    } else if (userRating == -2) {
      key = KeyEvent.VK_EQUALS;
    }
    putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(key, InputEvent.CTRL_DOWN_MASK));
  }

  @Override
  protected void processAction(ActionEvent e) {
    List<Movie> selectedMovies = MovieUIModule.getInstance().getSelectionModel().getSelectedMovies();

    if (selectedMovies.isEmpty()) {
      JOptionPane.showMessageDialog(MainWindow.getInstance(), TmmResourceBundle.getString("tmm.nothingselected"));
      return;
    }

    // set selected movies
    for (Movie movie : selectedMovies) {
      if (userRating == 0) {
        movie.removeRating(MediaRating.USER);
        return;
      }

      MediaRating current = movie.getUserRating();
      double initUserRating;
      if (userRating > 0) {
        initUserRating = userRating;
      } else if (current == MediaMetadata.EMPTY_RATING) {
        initUserRating = 6;
      } else {
        initUserRating = current.getRating();
        if (userRating == -1) {
          initUserRating--;
        } else if (userRating == -2) {
          initUserRating++;
        }
      }
      Map<String, MediaRating> newRatings = new HashMap<>();
      newRatings.put(MediaRating.USER, new MediaRating(MediaRating.USER, initUserRating, 1, 10));
      movie.setRatings(newRatings);

      movie.writeNFO();
      movie.saveToDb();
    }
  }
}
